## HTPP caching proxy using Twisted  
### Test task for Intel 471

Usage:
```
     git clone https://omojr@bitbucket.org/omojr/proxy-cache.git
     cd proxy-cache
     virtualenv -p python2 venv/
     pip install -r requinrements.txt
     python proxy_cache.py <max-request-size(MB)>
```
Runs on port 8080.

Includes small Flask app to easily test proxy with different request sizes.  
Usage:
```
    python test_server.py
```
Runs on port 5000. To get a response size of 100kb, send request to `http://localhost:5000/100`