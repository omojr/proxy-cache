from datetime import datetime, timedelta
from twisted.internet import reactor
from twisted.web import http, proxy

from apscheduler.schedulers.twisted import TwistedScheduler


class Cache:

    def __init__(self, max_size):
        """
        max_size -> int : max size of value in megabytes
        """
        self.cache = {}
        self.max_size = 1024 * max_size

    def get(self, key, default=None):
        return self.cache.get(key, default)

    def set(self, key, value, expire=15):
        if len(value) > self.max_size:
            return False, 'size'
        self.cache[key] = value
        scheduler.add_job(self.delete,
                          'date',
                          args=(key,),
                          id=key,
                          run_date=datetime.now()+timedelta(minutes=expire))
        return True,

    def append(self, key, value):
        if key not in self.cache.keys():
            return False, 'key'
        new_value = self.cache[key] + value
        if len(new_value) > self.max_size:
            return False, 'size'
        self.cache[key] = new_value
        return True,

    def delete(self, key):
        del self.cache[key]
        return True,


class ProxyClient(proxy.ProxyClient):
    _request_hash = None

    def handleResponsePart(self, buffer):
        request_hash = hash(self.father)
        if self._request_hash is None:
            self._request_hash = request_hash
            result = cache.set(self.father.uri, buffer)
        else:
            result = cache.append(self.father.uri, buffer)
        if not result[0] and result[1] == 'size':
            self.father.fail(413, 'Payload Too Large. Unable to process request.')
        else:
            proxy.ProxyClient.handleResponsePart(self, buffer)


class ProxyClientFactory(proxy.ProxyClientFactory):
    protocol = ProxyClient


class ProxyRequest(proxy.ProxyRequest):
    protocols = dict(http=ProxyClientFactory)

    def process(self):
        cached_resp = cache.get(self.uri)
        if cached_resp is None:
            proxy.ProxyRequest.process(self)
        else:
            self.fail(200, cached_resp)

    def fail(self, code, body):
        self.setResponseCode(code)
        self.responseHeaders.addRawHeader('Content-Type', 'text/html')
        self.write(body)
        self.finish()


class Proxy(proxy.Proxy):
    requestFactory = ProxyRequest


class ProxyFactory(http.HTTPFactory):
    protocol = Proxy


if __name__ == '__main__':
    import sys
    try:
        max_size = int(sys.argv[1])
    except ValueError:
        print "Wrong argument type, using default value 20MB"
        max_size = 20
    except IndexError:
        print "No arguments passed, using default value 20MB"
        max_size = 20

    cache = Cache(max_size)
    scheduler = TwistedScheduler()
    scheduler.start()

    try:
        reactor.listenTCP(8080, ProxyFactory())
        reactor.run()
    except (KeyboardInterrupt, SystemExit):
        pass
