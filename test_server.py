from time import sleep
import string
import random
from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return random.choice(string.ascii_letters + string.digits)


@app.route('/<int:size>')
def default(size):
    sleep(2)
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))


if __name__ == '__main__':
    app.run()
